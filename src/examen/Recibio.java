/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen;

/**
 *
 * @author PC
 */
public class Recibio {
     public int numrecibo = 0;
    public String fecha = "";
    public String nombre = "";
    public String domicilio = "";
    public int tiposervicio = 0;
    public float costopork = 0.0f;

    public double kilowattsc=0.0f;
    
    public Recibio(){
     this.numrecibo=0;
    this.fecha="";
    this.nombre="";
    this.domicilio="";
    this.tiposervicio =tiposervicio;
    this. costopork = 0.0f;
    this.kilowattsc=0;
        
    }
    
       public Recibio(int numrecibo,String fecha , String nombre, String domicilio, int tiposervicio, float costopork, float kilowattsc){
   this.numrecibo=numrecibo;
    this.fecha=fecha;
    this.nombre=nombre;
    this.domicilio=domicilio;
    this.tiposervicio =tiposervicio;
    this. costopork = costopork;
    this.kilowattsc=kilowattsc;
    }
           public Recibio(Recibio otro){
    this.numrecibo=otro.numrecibo;
    this.fecha=otro.fecha;
    this.nombre=otro.nombre;
    this.domicilio=otro.domicilio;
     this.tiposervicio =otro.tiposervicio;
    this. costopork = otro.costopork;
    this.kilowattsc=otro.kilowattsc;
    }

    public int getNumrecibo() {
        return numrecibo;
    }

    public void setNumrecibo(int numrecibo) {
        this.numrecibo = numrecibo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getTiposervicio() {
        return tiposervicio;
    }

    public void setTiposervicio(int tiposervicio) {
        this.tiposervicio = tiposervicio;
    }

    public float getCostopork() {
        return costopork;
    }

    public void setCostopork(float costopork) {
        this.costopork = costopork;
    }

    public double getKilowattsc() {
        return kilowattsc;
    }

    public void setKilowattsc(double kilowattsc) {
        this.kilowattsc = kilowattsc;
    }
           
               public float calcularSubtotal(){
    

     if(tiposervicio==1){costopork=(float) 2.00;}
     if(tiposervicio==2){costopork=(float) 3.00;}
     if(tiposervicio==3){costopork=(float) 5.00;}
     return (float) ( costopork* kilowattsc);

    }
                public float calcularImpuesto(){
    return (float) (calcularSubtotal()*0.16);
    }
                 public float calcularTotal(){
        return calcularSubtotal()+calcularImpuesto();
    }
    
}